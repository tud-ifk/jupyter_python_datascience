# Cannot activate `workshop_env`

- you started Jupyter Lab
- the first notebook opened automatically
- but:
    - you cannot select `01_intro_env`, for some reason
    - as a result, any code cell returns an error
    
## Solution

One likely cause is that the Kernel of the `01_intro_env` has not been properly initialized in user space.

Try the following steps:

### 1. Execute the first cell

- this will link the central `01_intro_env` to your user space
- you should see `Installed kernelspec 01_intro_env in /home/h5/s...` when it is finished

This is a video of the process:

![Link Kernel](assets/08_environ.webm)

### 2. Select the kernel

- On the top-right corner in jupyter lab, click on "Python 3"
- in the menu, click on the drop-down and select `workshop_env`
- if you do not see `workshop_env`, click cancel, wait a few seconds (or press <kbd>F5, and try to select it again

This is a video (note that below, the `workshop_env` has a different name):

![Select Kernel](assets/assets_sel_kernel_env.webm)

This is a screenshot of the selection screen:  

<img src="assets/assets_sel_kernel_env.png"  width="400">