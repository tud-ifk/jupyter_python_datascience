# 500: Internal Server Error

<img src="assets/error_500.png"  width="400">

Currently, we cannot fully locate this issue, but we are in contact with the ZIH.

It appears to be an issue with the initialization through prepared configuration. 
Once you see this issue, any further attempt to start the server through the 
spawn link will fail.

<details><summary>Background</summary>
    Here's a little bit more background on what happens if you click on the spawn link.
    <div>    
        <ul>
            <li>The spawn link provides several configuration parameters</li>
            <li>These will be used to start your server</li>
            <li>Among those are Number of CPUs, Memory, but also the project reference </li>
            <li>and a link to a Gitlab Repository that is cloned to local user space on startup</li>
            <li>Without this repository, you will not have no notebooks in your jupyter lab instance</li>
            <li>Under certain circumstances, this setup-link fails,</li>
            <li>and it leaves the environment in a broken state, preventing any further startups</li>
        </ul>
    </div>
</details>

## Workaround

One workaround is to "spawn" the server with default configurations,
shut it down afterwards and click the spawn link again.

Follow the steps below.

### 1. On the screen where it says "500: Internal Server Error"

- go to [Home](https://taurus.hrsk.tu-dresden.de/jupyter/hub/home)
- Click on the Blue Button that says "Start my Server"  

<img src="assets/start_manual.png"  width="400">
  
### 2. You'll get to the manual configuration screen. Select the following:  

- Advanced
- Enter `p_lv_mobicart_2021` in `Project (-A, --account)` field
- Select 2 CPU in `CPUs per task (-c, --cpus-per-task)`
- Click on "Spawn"

<img src="assets/spawn_manual_conf.png"  width="400">

### 3. Wait until the server started, and shut it down afterwards

- the explorer/files will be empty because no repsitory has been cloned on startup
- However, after you shut down this server, the spawn link should work again
- to shutdown the server, click on `File / Shut Down` in Jupyter 
  and on the next page click on "Stop my Server"

<img src="assets/stop_server.png"  width="400">

### 4. Go back to the [repository][repo], and click the spawn link again

- you should not see "500: Internal Server Error" this time
- if it appears again, the process above must be repeated

[repo]: https://gitlab.vgiscience.de/ad/mobile_cart_workshop2020/