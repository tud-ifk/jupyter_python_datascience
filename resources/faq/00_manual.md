## Manual startup

3. [Login to HPC][login-zih-link]
    - You'll be requested to login using your TUD Account
4. Select `Barnard - 2 core, 3G, 4 hours` or `Romeo - 2 core, 3G, 4 hours` and `p_lv_mobicart_2425` as the project name.
5. **Click Start** to spawn a Jupyter Lab instance on the TUD ZIH Jupyter Hub.
6. Clone the workshop files
   - Once JupyterHub started, open a new empty Python Notebook with click on the following icon:  
      ![Startup Jupyter Hub](resources/python_jup.webp)
    - .. and copy &paste the following command in the first cell:
    ```bash
    !cd /home/$USER && \
        git clone https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_python_datascience.git
    ```
    - run with <kbd>SHIFT+Enter</kbd>  
      ![Startup Jupyter Hub](resources/clone.webp)
    - this will download the workshop material to your local user folder (explorer on the left side)
    - in the explorer on the left, open the file `jupyter_python_datascience/notebooks/01_raw_intro.ipynb`  
      ![Startup Jupyter Hub](resources/notebook_jup.webp)
        - and start following the instructions  

[login-zih-link]: https://jupyterhub.hpc.tu-dresden.de/hub/