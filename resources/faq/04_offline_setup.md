# Working offline

Setup of python environments can be frustrating,  
particularly if you're working with Windows.  

There are many ways to setup python environments,  
and which way is best often depends on the context.  

If you're familiar with python and package managers,  
you can use the list of requirements specified the `yml` files in  
[resources/](../) to create your env using any available approach.

For all other users, we either suggest the conda  
or docker approach described below.

If you're on Windows, the approach should be the same.  
However, on Windows, we suggest using  
[Windows Subsystem for Linux (WSL1)][wsl].

Follow any guide to set up WSL1. One example  
process is describes [here][wsl-guide].

## (Mini)conda


We suggest following the [Miniconda](https://docs.conda.io/en/latest/miniconda.html) installer,  
instead of the Anaconda distribution. A description for setup in WSL [is available here][wsl-conda].

If you do not work in [WSL][wsl], we suggest installation of  
Miniconda using [Chocolatey Windows Package Manager][choco]  
(elevated windows command line):
```cmd
choco install miniconda3
```

Afterwards download or clone the repository,  
and create a python environment from the file(s)  
provided in the repo:
```bash
git clone https://gitlab.hrz.tu-chemnitz.de/tud-ifk/python_datascience_2022.git
cd python_datascience_2022
# not necessary, but recommended:
conda config --env --set channel_priority strict
conda env create -f resources/01_intro.yml
```
A shortcut may be to directly install using the exact requirements:

```bash
conda create \
    --name intro_env \
    --file resources/01_intro.txt
```

This will usually go faster, since conda does not have to "solve" the list of dependencies.

Since we are using a separate Jupyter Lab environment,  
you must install those dependencies manually.
```bash
conda activate intro_env
conda install -c conda-forge ipywidgets \
    jupyter_contrib_nbextensions \
    jupyter_nbextensions_configurator \
    jupyterlab \
    jupytext \
    nbconvert
```

Note: On Windows, use `^` instead of `\`  
as the multi-line-command character

Now, activate the environment and start jupyter lab:
```
jupyter lab
```

and open your webbrowser at the default URL (`http://localhost:8888/`)

## IfK Jupyter Docker Container

We have prepared a Docker container that can be used  
to serve Jupyter Lab with various environments.

First, [setup Docker](https://docs.docker.com/docker-for-windows/install/).  
We also suggest using [Chocolatey Windows Package Manager][choco]  
(elevated windows command line):
```cmd
choco install docker-desktop
```

See a complete list of instructions [here][wsl-docker].

You can use Windows Command Line, but we do suggest using  
[Windows Subsystem for Linux (WSL1)][wsl]  
for starting Docker Containers in Windows.

Afterwards, clone the [IfK JupyterLab Docker Container](https://gitlab.vgiscience.de/lbsn/tools/jupyterlab).

```bash
git clone https://gitlab.vgiscience.de/lbsn/tools/jupyterlab.git
cd jupyterlab
```

Create a folder `envs` and copy [01_intro.yml](../01_intro.yml) to this folder.
```bash
mkdir envs
cd envs
wget https://gitlab.hrz.tu-chemnitz.de/tud-ifk/python_datascience_2022/-/raw/main/resources/01_intro.yml
```

Adjust the `.env.example` to specify that environment_default.yml should be used:
```
cd ..
cp .env.example .env
```

Edit the `.env` file and add:
```yml
ENVIRONMENT_FILE=envs/01_intro.yml
WORKER_ENV_NAME=intro_env
JUPYTER_NOTEBOOKS=/c/mypath/to/python_datascience_2022/
```

Build and startup the docker container:
```bash
docker-compose up -d
```

Go to http://localhost:8888/ and login with the password from `.env`.

[wsl]: https://docs.microsoft.com/de-de/windows/wsl/install-win10
[wsl-guide]: https://ad.vgiscience.org/links/posts/2019-05-27-setup-wsl-win10/
[wsl-conda]: https://ad.vgiscience.org/links/posts/2019-05-27-wsl-software/#miniconda3
[wsl-docker]: https://ad.vgiscience.org/links/posts/2019-05-27-setup-wsl-win10/#install-docker
[choco]: https://chocolatey.org/