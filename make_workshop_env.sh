#!/bin/sh

module load Anaconda3
conda config --set channel_priority strict
cd /home/$USER
mkdir workshop_env_user
conda env create \
    --prefix /home/$USER/workshop_env_user/workshop_env_user \
    --file /home/$USER/python-spatial-datascience-course.git/environment_default.yml \
    --quiet
source /sw/installed/Anaconda3/2019.03/bin/activate /home/$USER/workshop_env_user/workshop_env_user
conda install ipykernel --channel conda-forge
/home/$USER/workshop_env_user/workshop_env_user/bin/python \
    -m ipykernel install \
    --user \
    --name workshop_env_user \
    --display-name="workshop_env_user"
