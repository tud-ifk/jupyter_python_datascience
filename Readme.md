[![version](https://kartographie.geo.tu-dresden.de/ad/jupyter_python_datascience/version.svg)][static-gl-url] [![pipeline](https://kartographie.geo.tu-dresden.de/ad/jupyter_python_datascience/pipeline.svg)][static-gl-url]

# Mobile Cartography 2024/25 Workshop Series: Social Media, Jupyter Lab & Tag Maps

![Cover Figures](https://ad.vgiscience.org/links/imgs/2019-05-23_emojimap_campus.png)

This is the public repository containing the course material for the
Mobile Cartography Jupyter Lab Workshop.

------

## Preparations

1. If working from remote: Make sure to connect to TUD network using [a VPN connection][cisco-zih] (Cisco VPN or OpenVPN), otherwise you will not be able to use any of ZIH HPC Services  
2. Add your ZIH-Login to the HPC project, using the pre-shared link (see email)
    - this needs to be done only once  
    - it may take up to two hours before your login is available

## Automatic startup Jupyter Lab

3. [Login to HPC][login-zih-link] (do not click "spawn")
    - You'll be requested to login using your TUD Account
4. **Click [this link][spawn-zih-link]** to spawn a Jupyter Lab instance on the TUD ZIH Jupyter Hub.  
    - If you see a pop-up asking "Found slurm parameters in URL. Do you want to submit and spawn now?" - click **Ok**
    - Do _not_ click "Start My Server".
        - If you see a button or form, click or copy the link again, paste, & the server should start automatically.
    - **Important: Do not change configuration if you see a form:** If the Server doesn't start automatically, click the [ZIH Jupyter link][spawn-zih-link] one more time
    - If there are not enough resources and you need to wait a very long time (>10 minutes), try [this alternative spawn link][spawn-zih-link2] that uses a different ZIH resource partition

A video showing startup and environment selection:

![Startup Jupyter Hub](resources/startup_hub.webm)

Afterwards, once the notebook has opened:  
- If asked to select a Kernel: Confirm suggestion (Python 3) with "Select".  
- execute the first cell, with `SHIFT+ENTER`  
- this will link the conda `01_intro_env` to your user folder. Follow any instructions in the notebook.  

## Manual startup

See [00_manual.md](resources/faq/00_manual.md) with a description to start Jupyter Hub manually,
without the spawn link.

## FAQ

Based on previous workshops, 
we have collected some frequent questions and answers.

Please click on the links below to see possible routes to solve these issues.

1. [**500: Internal Server Error** after clicking spawn the link?](resources/faq/01_error_500.md)
2. [Jupyter Lab Starts, but **the explorer on the left is empty**?](resources/faq/02_explorer_empty.md)
3. [**Cannot activate `workshop_env`**/ the first code cell returns an error?](resources/faq/03_activate_workshop_env.md)
4. [How can I run these notebooks offline (locally)?](resources/faq/04_offline_setup.md)

## HTML Versions of Notebooks

1. [01_raw_intro.html](https://kartographie.geo.tu-dresden.de/ad/jupyter_python_datascience/01_raw_intro.html)
2. [02_hll_intro.html](https://kartographie.geo.tu-dresden.de/ad/jupyter_python_datascience/02_hll_intro.html)
3. [03_tagmaps.html](https://kartographie.geo.tu-dresden.de/ad/jupyter_python_datascience/03_tagmaps.html)
4. [04_topic_classification.html](https://kartographie.geo.tu-dresden.de/ad/jupyter_python_datascience/04_topic_classification.html)

[login-zih-link]: https://jupyterhub.hpc.tu-dresden.de/hub/
[spawn-zih-link]: https://jupyterhub.hpc.tu-dresden.de/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2Ftud-ifk%2Fjupyter_python_datascience.git&branch=main&urlpath=lab%2Ftree%2Fjupyter_python_datascience.git%2Fnotebooks%2F01_raw_intro.ipynb#/~(cluster~'barnard~nodes~'2~project~'p_lv_mobicart_2425~runtime~'2*3a30*3a00~cpuspertask~'2~ntasks~'2~mempercpu~'4096)
[spawn-zih-link2]: https://jupyterhub.hpc.tu-dresden.de/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2Ftud-ifk%2Fjupyter_python_datascience.git&branch=main&urlpath=lab%2Ftree%2Fjupyter_python_datascience.git%2Fnotebooks%2F01_raw_intro.ipynb#/~(cluster~'romeo~nodes~'2~project~'p_lv_mobicart_2425~runtime~'2*3a30*3a00~cpuspertask~'2~ntasks~'2~mempercpu~'2583)
[static-gl-url]: https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_python_datascience
[cisco-zih]: https://tu-dresden.de/zih/dienste/service-katalog/arbeitsumgebung/zugang_datennetz/vpn?set_language=de
