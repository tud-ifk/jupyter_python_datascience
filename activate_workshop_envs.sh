#!/bin/sh
# Make central workerhop_envs
# available in user jupyter lab kernel spec
# e.g.
# /home/h5/$USER/.local/share/jupyter/kernels/intro_env

PROJECT_ENV_DIR="/data/horse/ws/s7398234-p_lv_mobicart_2425"

if [ ! -d "$PROJECT_ENV_DIR" ]; then
  echo "$PROJECT_ENV_DIR does not exist. If you are running this workshop locally, please install and link workshop environments yourself."
  exit 1
fi

$PROJECT_ENV_DIR/intro_env/bin/python \
    -m ipykernel install \
    --user \
    --name intro_env \
    --display-name="01_intro_env"

$PROJECT_ENV_DIR/hll_env/bin/python \
    -m ipykernel install \
    --user \
    --name hll_env \
    --display-name="02_hll_env"

$PROJECT_ENV_DIR/tagmaps_env/bin/python \
    -m ipykernel install \
    --user \
    --name tagmaps_env \
    --display-name="03_tagmaps_env"

$PROJECT_ENV_DIR/topics_env/bin/python \
    -m ipykernel install \
    --user \
    --name topics_env \
    --display-name="04_topics_env"

# install jupytext in user mode
# /sw/installed/Anaconda3/2019.03/bin/python \
#     -m pip install jupytext \
#     --upgrade \
#     --user