"""YFCC grid plot

Source:
https://ad.vgiscience.org/yfcc_gridagg/
"""

from pathlib import Path
from typing import Dict, List, Optional, Tuple

import geopandas as gp
import geoviews as gv
import holoviews as hv
import mapclassify as mc
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import geoviews.feature as gf
from bokeh.models import FixedTicker, HoverTool
from cartopy import crs
from matplotlib import colors


def format_bound(
    upper_bound: float = None, lower_bound: float = None) -> str:
    """Format legend text for class bounds"""
    if upper_bound is None:
        return f'{lower_bound:,.0f}'
    if lower_bound is None:
        return f'{upper_bound:,.0f}'
    return f'{lower_bound:,.0f} - {upper_bound:,.0f}'

def get_label_bounds(
    scheme_classes, metric_series: pd.Series,
    flat: bool = None) -> List[str]:
    """Get all upper bounds in the scheme_classes category"""
    upper_bounds = scheme_classes.bins
    # get and format all bounds
    bounds = []
    for idx, upper_bound in enumerate(upper_bounds):
        if idx == 0:
            lower_bound = metric_series.min()
        else:
            lower_bound = upper_bounds[idx-1]
        if flat:
            bound = format_bound(
                lower_bound=lower_bound)
        else:
            bound = format_bound(
                upper_bound, lower_bound)
        bounds.append(bound)
    if flat:
        upper_bound = format_bound(
            upper_bound=upper_bounds[-1])
        bounds.append(upper_bound)
    return bounds

def label_nodata_xr(
        grid: gp.GeoDataFrame, inverse: bool = None,
        metric: str = "postcount_est", scheme: str = "HeadTailBreaks",
        label_nonesignificant: bool = None, cmap_name: str = "OrRd"):
    """Create a classified colormap from pandas dataframe
    column with additional No Data-label

    Args:
        grid: A geopandas geodataframe with metric column to classify
        inverse: If True, colors are inverted (dark)
        metric: The metric column to classify values
        scheme: The classification scheme to use.
        label_nonesignificant: If True, adds an additional
            color label for none-significant values based
            on column "significant"
        cmap_name: The colormap to use.

    Adapted from:
        https://stackoverflow.com/a/58160985/4556479
    See available colormaps:
        http://holoviews.org/user_guide/Colormaps.html
    See available classification schemes:
        https://pysal.org/mapclassify/api.html
    """
    # get headtail_breaks
    # excluding NaN values
    grid_nan = grid[metric].replace(0, np.nan)
    # get classifier scheme by name
    classifier = getattr(mc, scheme)
    # some classification schemes (e.g. HeadTailBreaks)
    # do not support specifying the number of classes returned
    # construct optional kwargs with k == number of classes
    optional_kwargs = {"k":9}
    if scheme == "HeadTailBreaks":
        optional_kwargs = {}
    # explicitly set dtype to float,
    # otherwise mapclassify will error out due
    # to unhashable type numpy ndarray
    # from object-column
    scheme_breaks = classifier(
        grid_nan.dropna().astype(float), **optional_kwargs)
    # set breaks category column
    # to spare cats:
    # increase by 1, 0-cat == NoData/none-significant
    spare_cats = 1
    grid[f'{metric}_cat'] = scheme_breaks.find_bin(
        grid_nan) + spare_cats
    # set cat 0 to NaN values
    # to render NoData-cat as transparent
    grid.loc[grid_nan.isnull(), f'{metric}_cat'] = np.nan
    # get label bounds as flat array
    bounds = get_label_bounds(
        scheme_breaks, grid_nan.dropna().values, flat=True)
    cmap_name = cmap_name
    cmap = plt.cm.get_cmap(cmap_name, scheme_breaks.k)
    # get hex values
    cmap_list = [colors.rgb2hex(cmap(i)) for i in range(cmap.N)]
    # prepend nodata color
    # shown as white in legend
    first_color_legend = '#ffffff'
    if inverse:
        first_color_legend = 'black'
    cmap_list = [first_color_legend] + cmap_list
    cmap_with_nodata = colors.ListedColormap(cmap_list)
    return cmap_with_nodata, bounds, scheme_breaks

def get_custom_tooltips(items: Dict[str, str]) -> List[Tuple[str, str]]:
    """Compile HoverTool tooltip formatting with items to show on hover"""
    # thousands delimitor formatting
    # will be applied to the for the following columns
    tdelim_format = [
        'usercount_est', 'postcount_est', 'userdays_est',
        'usercount', 'postcount', 'userdays']
    # in HoloViews, custom tooltip formatting can be
    # provided as a list of tuples (name, value)
    tooltips=[
        # f-strings explanation:
        # - k: the item name, v: the item value,
        # - @ means: get value from column
        # optional formatting is applied using
        # `"{,f}" if v in thousand_formats else ""`
        # - {,f} means: add comma as thousand delimiter
        # only for values in tdelim_format (usercount and postcount)
        # else apply automatic format
        (k,
         f'@{v}{"{,f}" if v.replace("_expected", "") in tdelim_format else ""}'
        ) for k, v in items.items()
    ]
    return tooltips

def set_active_tool(plot, element):
    """Enable wheel_zoom in bokeh plot by default"""
    plot.state.toolbar.active_scroll = plot.state.tools[0]

def convert_gdf_to_gvimage(
        grid, metric, cat_count,
        additional_items: Dict[str, str] = None) -> gv.Image:
    """Convert GeoDataFrame to gv.Image using categorized
    metric column as value dimension

    Args:
        grid: A geopandas geodataframe with indexes x and y
            (projected coordinates) and aggregate metric column
        metric: target column for value dimension.
            "_cat" will be added to retrieve classified values.
        cat_count: number of classes for value dimension
        additional_items: a dictionary with optional names
            and column references that are included in
            gv.Image to provide additional information
            (e.g. on hover)
    """
    if additional_items is None:
        additional_items_list = []
    else:
        additional_items_list = [
            v for v in additional_items.values()]
    # convert GeoDataFrame to xarray object
    # the first vdim is the value being used
    # to visualize classes on the map
    # include additional_items (postcount and usercount)
    # to show exact information through tooltip
    xa_dataset = gv.Dataset(
        grid.to_xarray(),
        vdims=[
            hv.Dimension(
                f'{metric}_cat', range=(0, cat_count))]
            + additional_items_list,
        crs=crs.Mollweide())
    return xa_dataset.to(gv.Image, crs=crs.Mollweide())

def plot_interactive(grid: gp.GeoDataFrame, title: str,
    metric: str = "postcount_est", store_html: str = None,
    additional_items: Dict[str, str] = {
        'Post Count':'postcount',
        'User Count':'usercount',
        'User Days':'userdays',
        'Post Count (estimated)':'postcount_est',
        'User Count (estimated)':'usercount_est',
        'User Days (estimated)':'userdays_est'},
    scheme: str = "HeadTailBreaks",
    cmap: str = "OrRd",
    inverse: bool = None,
    output: Optional[Path] = None):
    """Plot interactive map with holoviews/geoviews renderer

    Args:
        grid: A geopandas geodataframe with indexes x and y
            (projected coordinates) and aggregate metric column
        metric: target column for aggregate. Default: postcount.
        store_html: Provide a name to store figure as interactive HTML.
        title: Title of the map
        additional_items: additional items to show on hover
        scheme: The classification scheme to use. Default "HeadTailBreaks".
        cmap: The colormap to use. Default "OrRd".
        inverse: plot other map elements inverse (black)
        output: main folder (Path) for storing output
    """
    # check if all additional items are available
    for key, item in list(additional_items.items()):
        if item not in grid.columns:
            additional_items.pop(key)
    # work on a shallow copy to not modify original dataframe
    grid_plot = grid.copy()
    # classify metric column
    cmap_with_nodata, bounds, headtail_breaks = label_nodata_xr(
        grid=grid_plot, inverse=inverse, metric=metric,
        scheme=scheme, cmap_name=cmap)
    # construct legend labels for colormap
    label_dict = {}
    for i, s in enumerate(bounds):
        label_dict[i+1] = s
    label_dict[0] = "No data"
    cat_count = len(bounds)
    # create gv.image layer from gdf
    img_grid = convert_gdf_to_gvimage(
            grid=grid_plot,
            metric=metric, cat_count=cat_count,
            additional_items=additional_items)
    # define additional plotting parameters
    # width of static jupyter map,
    # 360° == 1200px
    width = 1200
    # height of static jupyter map,
    # 360°/2 == 180° == 600px
    height = int(width/2)
    responsive = False
    aspect = None
    # if stored as html,
    # override values
    if store_html:
        width = None
        height = None
        responsive = True
    # define width and height as optional parameters
    # only used when plotting inside jupyter
    optional_kwargs = dict(width=width, height=height)
    # compile only values that are not None into kwargs-dict
    # by using dict-comprehension
    optional_kwargs_unpack = {
        k: v for k, v in optional_kwargs.items() if v is not None}
    # prepare custom HoverTool
    tooltips = get_custom_tooltips(
        additional_items)
    hover = HoverTool(tooltips=tooltips)
    # create image layer
    image_layer = img_grid.opts(
            color_levels=cat_count,
            cmap=cmap_with_nodata,
            colorbar=True,
            clipping_colors={'NaN': 'transparent'},
            colorbar_opts={
                # 'formatter': formatter,
                'major_label_text_align':'left',
                'major_label_overrides': label_dict,
                'ticker': FixedTicker(
                    ticks=list(range(0, len(label_dict)))),
                },
            tools=[hover],
            # optional unpack of width and height
            **optional_kwargs_unpack
        )
    edgecolor = 'black'
    bg_color = None
    fill_color = '#479AD4'
    alpha = 0.1
    if inverse:
        alpha = 1
        bg_color = 'black'
        edgecolor = 'white'
    # combine layers into single overlay
    # and set global plot options
    gv_layers = (gf.ocean.opts(alpha=alpha, fill_color=fill_color) * \
                 image_layer * \
                 gf.coastline.opts(line_color=edgecolor) * \
                 gf.borders.opts(line_color=edgecolor)
            ).opts(
        bgcolor=bg_color,
        # global_extent=True,
        projection=crs.Mollweide(),
        responsive=responsive,
        data_aspect=1, # maintain fixed aspect ratio during responsive resize
        hooks=[set_active_tool],
        title=title)
    if store_html and output:
        hv.save(gv_layers, output / f'{store_html}.html', backend='bokeh')
    else:
        return gv_layers

